#!/bin/bash

# xen specific
RAM=1024
CPU=2
DISK=50
XENBR=xenbr124
PASSWORD="PjvlVhiKaPYp"

# vm specific
NAME=memcache1-cnet.sportsmedia.nbg.rcnhost.com
DNS=8.8.8.8,8.8.4.4
IP=69.55.59.42
GW=69.55.59.33
MASK=255.255.255.224
KS="http://blog.msmsoft.info/kickstart.php?hostname=$NAME&swapsize=$RAM&password=$PASSWORD&ip=$IP&netmask=$MASK&gateway=$GW&nameserver=$DNS"
MIRROR=http://yum.realitychecknetwork.com/mirror/centos/6/os/x86_64/

KERNEL_OPTIONS="ksdevice=eth0 noipv6 gateway=$GW ip=$IP netmask=$MASK dns=$DNS ks=$KS"

virt-install --name=$NAME --ram=$RAM --vcpus=$CPU --os-type=linux --os-variant=rhel6 --paravirt --virt-type=xen  --nographics --noautoconsole --disk=path=/var/lib/xen/images/$NAME,size=$DISK --network=bridge=$XENBR --location=$MIRROR --extra-args="$KERNEL_OPTIONS" 

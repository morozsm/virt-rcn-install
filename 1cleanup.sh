NAME=$1
CONFIG_DIR=/etc/xen
LOCATION=/var/lib/xen/images

virsh dumpxml $NAME > /tmp/$NAME.xml
virsh domxml-to-native xen-xm /tmp/$NAME.xml > $CONFIG_DIR/$NAME
/bin/sed -i 's/, "mac.*xenbr16.*"//' $CONFIG_DIR/$NAME
/bin/sed -i 's/-disk0//' $CONFIG_DIR/$NAME
/bin/sed -i '/^maxmem.*$/d' $CONFIG_DIR/$NAME
/bin/sed -i 's/,script.*"/"/' $CONFIG_DIR/$NAME
ln -s $CONFIG_DIR/$NAME $CONFIG_DIR/auto/$NAME
#mv $LOCATION/$NAME-disk0 $LOCATION/$NAME
xm delete $NAME
chmod 644 $LOCATION/$NAME
